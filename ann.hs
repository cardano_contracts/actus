import qualified Prelude                       as HP
import qualified Language.PlutusTx             as PlutusTx
import           Language.PlutusTx.Prelude
import           Ledger                         ( Address
                                                , DataScript(DataScript)
                                                , PendingTx
                                                , PubKey
                                                , RedeemerScript(RedeemerScript)
                                                , TxId
                                                , ValidatorScript
                                                  ( ValidatorScript
                                                  )
                                                , compileScript
                                                , lifted
                                                , scriptAddress
                                                , Tx
                                                )
import qualified Ledger.Ada                    as Ada
import           Ledger.Ada                     ( Ada )
import qualified Ledger.Interval               as Interval
import           Ledger.Slot                    ( Slot
                                                , SlotRange
                                                )
import qualified Ledger.Validation             as V
import           Ledger.Value                   ( Value )
import qualified Ledger.Value                  as Value
import           Playground.Contract
import           Wallet                         ( EventHandler(EventHandler)
                                                , EventTrigger
                                                , MonadWallet
                                                , collectFromScript
                                                , fundsAtAddressGeqT
                                                , logMsg
                                                , neverT
                                                , defaultSlotRange
                                                , payToPublicKey_
                                                , intervalFrom
                                                , ownPubKey
                                                , payToScript_
                                                , payToScript
                                                , register
                                                , registerOnce
                                                , slotRangeT
                                                , slot
                                                , startWatching
                                                )
import qualified Wallet                        as W
import           Wallet.Emulator                ( Wallet )
import qualified Wallet.Emulator               as EM

type LoanAmount = Ada
type Frequency = Slot
type InterestRate = Integer
type InstallmentsCount = Integer

data Loan = Loan
    { notionalPrincipality :: LoanAmount
    , nominalInterestRate :: InterestRate
    , interestCycle :: Frequency
    , installmentCount :: InstallmentsCount
    , loaneePubKey :: PubKey
    } deriving (Generic, ToJSON, FromJSON)

PlutusTx.makeLift ''Loan

makeLoan :: LoanAmount -> InterestRate -> Frequency -> InstallmentsCount -> PubKey -> Loan
makeLoan notionalPrincipality nominalInterestRate interestCycle installmentCount loaneePubKey
  = Loan { notionalPrincipality = notionalPrincipality
         , nominalInterestRate  = nominalInterestRate
         , interestCycle        = interestCycle
         , installmentCount     = installmentCount
         , loaneePubKey         = loaneePubKey
         }

loanDataScript :: Loan -> DataScript
loanDataScript = DataScript . Ledger.lifted

loanRedeemerScript :: LoanAmount -> RedeemerScript
loanRedeemerScript = RedeemerScript. Ledger.lifted

isLoanAmountCorrect :: Loan -> LoanAmount -> Bool
isLoanAmountCorrect Loan{notionalPrincipality = requestedLoanAmount} 
                    amountLended = requestedLoanAmount == amountLended

validateLoan :: Loan -> LoanAmount -> PendingTx -> Bool
validateLoan loan amountLended _ = isLoanAmountCorrect loan amountLended

loanValidator :: ValidatorScript
loanValidator = ValidatorScript $
  $$(Ledger.compileScript [|| validateLoan ||])

loanAddress :: Address
loanAddress = Ledger.scriptAddress loanValidator

lendLoan :: MonadWallet m => LoanAmount -> InterestRate -> Frequency -> InstallmentsCount -> m ()
lendLoan notionalPrincipality nominalInterestRate interestCycle installmentCount = do
    pubKey <- ownPubKey
    let loan = makeLoan notionalPrincipality
                        nominalInterestRate
                        interestCycle
                        installmentCount
                        pubKey
    payToScript_ defaultSlotRange
                 loanAddress
                 (Ada.toValue notionalPrincipality)
                 (loanDataScript loan)

requestLoan :: MonadWallet m => LoanAmount -> InterestRate -> Frequency -> InstallmentsCount -> Wallet -> m ()
requestLoan notionalPrincipality nominalInterestRate interestCycle installmentCount wallet = do
    let pubKey = EM.walletPubKey wallet
        loan   = makeLoan notionalPrincipality
                          nominalInterestRate
                          interestCycle
                          installmentCount
                          pubKey
    startWatching loanAddress
    registerOnce (getLoanTrigger loan) (getLoanHandler loan)

getLoanTrigger :: Loan -> EventTrigger
getLoanTrigger Loan{notionalPrincipality = requestedLoanAmount} = 
  fundsAtAddressGeqT loanAddress (Ada.toValue requestedLoanAmount)

getLoanHandler :: MonadWallet m => Loan -> EventHandler m
getLoanHandler loan = EventHandler $ \_ -> do
  let totalInstallments = installmentCount loan
      adaToPay          = calculateInstallmentValue loan
  
  collectLoan
  registerPayback adaToPay totalInstallments loan

 where
  collectLoan =
    let redeemerScript = loanRedeemerScript (notionalPrincipality loan)
    in  collectFromScript defaultSlotRange loanValidator redeemerScript

registerPayback :: MonadWallet m => Ada -> InstallmentsCount -> Loan -> m ()
registerPayback adaToPay installments loan = do
  currentSlot <- slot
  registerOnce (getInstallmentTrigger installments loan currentSlot)
               (getInstallmentHandler adaToPay installments loan)

getInstallmentTrigger :: Integer -> Loan -> Slot -> EventTrigger
getInstallmentTrigger installmentsLeft loan slot
  | installmentsLeft > 0 = slotRangeT (intervalFrom $ interestCycle loan + slot)
  | otherwise            = neverT

getInstallmentHandler :: MonadWallet m => Ada -> Integer -> Loan -> EventHandler m
getInstallmentHandler adaToPay currRemainingInstallments loan = EventHandler $ \_ -> do
    let newRemainingInstallments = currRemainingInstallments - 1
    
    payInstallment adaToPay
    registerPayback adaToPay newRemainingInstallments loan

 where
  payInstallment toPay =
    payToPublicKey_ defaultSlotRange (Ada.toValue toPay) (loaneePubKey loan)

calculateInstallmentValue :: Loan -> Ada
calculateInstallmentValue Loan{ notionalPrincipality = requestedLoanAmount
                              , nominalInterestRate = interestRate
                              , installmentCount = totalInstallments} = 
  let c = adaToPreludeFloat requestedLoanAmount         -- Total loan amount
      r = (fromIntegral interestRate) `preludeDiv` 100  -- Interest rate
      n = fromIntegral totalInstallments                -- Number of payments
      rn = r `preludeDiv` 12
  in
    Ada.lovelaceOf . ceiling $
      (c `preludeMult` rn)
      `preludeDiv`
      (1 `preludeSub` ((1 `preludeAdd` rn) `preludePow` (- n)))

adaToPreludeFloat :: Ada -> HP.Float
adaToPreludeFloat = fromIntegral . toInteger

preludeAdd :: HP.Num a => a -> a -> a
preludeAdd = (HP.+)

preludeMult :: HP.Num a => a -> a -> a
preludeMult = (HP.*)

preludeSub :: HP.Num a => a -> a -> a
preludeSub = (HP.-)

preludePow :: (HP.Num a, HP.Floating a) => a -> a -> a
preludePow = (HP.**)

preludeDiv :: (HP.Num a, HP.Floating a) => a -> a -> a
preludeDiv = (HP./)

$(mkFunctions
    ['lendLoan,
     'requestLoan
    ])