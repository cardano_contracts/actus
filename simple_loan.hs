import qualified Prelude                       as HP
import qualified Language.PlutusTx             as PlutusTx
import           Language.PlutusTx.Prelude
import           Ledger                         ( Address
                                                , DataScript(DataScript)
                                                , PendingTx
                                                , PubKey
                                                , RedeemerScript(RedeemerScript)
                                                , TxId
                                                , ValidatorScript
                                                  ( ValidatorScript
                                                  )
                                                , applyScript
                                                , compileScript
                                                , hashTx
                                                , lifted
                                                , pendingTxValidRange
                                                , scriptAddress
                                                , valueSpent
                                                , Tx
                                                )
import qualified Ledger.Ada                    as Ada
import           Ledger.Ada                     ( Ada )
import qualified Ledger.Interval               as Interval
import           Ledger.Slot                    ( Slot
                                                , SlotRange
                                                )
import qualified Ledger.Validation             as V
import           Ledger.Value                   ( Value )
import qualified Ledger.Value                  as Value
import           Playground.Contract
import           Wallet                         ( EventHandler(EventHandler)
                                                , EventTrigger
                                                , MonadWallet
                                                , andT
                                                , collectFromScript
                                                , collectFromScriptTxn
                                                , fundsAtAddressGeqT
                                                , logMsg
                                                , neverT
                                                , defaultSlotRange
                                                , payToPublicKey_
                                                , intervalFrom
                                                , ownPubKey
                                                , payToScript_
                                                , payToScript
                                                , register
                                                , registerOnce
                                                , slotRangeT
                                                , slot
                                                , startWatching
                                                )
import qualified Wallet                        as W
import           Wallet.Emulator                ( Wallet )
import qualified Wallet.Emulator               as EM
import           Data.Text                     as T

type InterestRate = Integer
type InstallmentsCount = Integer

data Loan = Loan
    { notionalPrincipality :: Ada
    , nominalInterestRate :: InterestRate
    , interestCycle :: Slot
    , installmentCount :: InstallmentsCount
    , loaneePubKey :: PubKey
    } deriving (Generic, ToJSON, FromJSON)

PlutusTx.makeLift ''Loan

makeLoan :: Ada -> InterestRate -> Slot -> InstallmentsCount -> PubKey -> Loan
makeLoan notionalPrincipality nominalInterestRate interestCycle installmentCount loaneePubKey
  = Loan { notionalPrincipality = notionalPrincipality
         , nominalInterestRate  = nominalInterestRate
         , interestCycle        = interestCycle
         , installmentCount     = installmentCount
         , loaneePubKey         = loaneePubKey
         }

loanDataScript :: Loan -> DataScript
loanDataScript loan = DataScript (Ledger.lifted loan)

loanRedeemerScript :: Ada -> RedeemerScript
loanRedeemerScript ada = RedeemerScript (Ledger.lifted ada)

isLoanAmountCorrect :: Loan -> Ada -> Bool
isLoanAmountCorrect loan amount = notionalPrincipality loan == amount

validateLoan :: Loan -> Ada -> PendingTx -> Bool
validateLoan loan amountLended _ = loan `isLoanAmountCorrect` amountLended

loanValidator :: ValidatorScript
loanValidator = ValidatorScript $
  $$(Ledger.compileScript [|| validateLoan ||])

loanAddress :: Address
loanAddress = Ledger.scriptAddress loanValidator

lendLoan
  :: MonadWallet m => Ada -> InterestRate -> Slot -> InstallmentsCount -> m ()
lendLoan notionalPrincipality nominalInterestRate interestCycle installmentCount
  = do
    pubKey <- ownPubKey
    let loan = makeLoan notionalPrincipality
                        nominalInterestRate
                        interestCycle
                        installmentCount
                        pubKey
    payToScript_ defaultSlotRange
                 loanAddress
                 (Ada.toValue notionalPrincipality)
                 (loanDataScript loan)

requestLoan
  :: MonadWallet m
  => Wallet
  -> Ada
  -> InterestRate
  -> Slot
  -> InstallmentsCount
  -> m ()
requestLoan wallet notionalPrincipality nominalInterestRate interestCycle installmentCount
  = do
    let pubKey = EM.walletPubKey wallet
        loan   = makeLoan notionalPrincipality
                          nominalInterestRate
                          interestCycle
                          installmentCount
                          pubKey
    startWatching loanAddress
    registerOnce (getLoanTrigger loan) (getLoanHandler loan)

getLoanTrigger :: Loan -> EventTrigger
getLoanTrigger loan =
  fundsAtAddressGeqT loanAddress (Ada.toValue (notionalPrincipality loan))

getLoanHandler :: MonadWallet m => Loan -> EventHandler m
getLoanHandler loan = EventHandler $ \_ -> do
  let totalWithInterest =
        notionalPrincipality loan `withInterestRounded` nominalInterestRate loan
      totalInstallments = installmentCount loan

  redeemLoan
  registerPaymentTrigger totalWithInterest totalInstallments loan

 where
  redeemLoan =
    let redeemerScript = loanRedeemerScript (notionalPrincipality loan)
    in  collectFromScript defaultSlotRange loanValidator redeemerScript

registerPaymentTrigger
  :: MonadWallet m => Ada -> InstallmentsCount -> Loan -> m ()
registerPaymentTrigger adaToPay installments loan = do
  currentSlot <- slot
  registerOnce (getInstallmentTrigger installments loan currentSlot)
               (getInstallmentHandler adaToPay installments loan)

getInstallmentTrigger :: Integer -> Loan -> Slot -> EventTrigger
getInstallmentTrigger installmentsLeft loan slot
  | installmentsLeft > 0 = slotRangeT (intervalFrom $ interestCycle loan + slot)
  | otherwise            = neverT

getInstallmentHandler
  :: MonadWallet m => Ada -> Integer -> Loan -> EventHandler m
getInstallmentHandler currRemainingAmount currRemainingInstallments loan =
  EventHandler $ \_ -> do
    let installmentValue = calculateInstallmentValue
          currRemainingAmount
          currRemainingInstallments
          (nominalInterestRate loan)
        newRemainingAmount       = currRemainingAmount - installmentValue
        newRemainingInstallments = currRemainingInstallments - 1

    payInstallment installmentValue
    registerPaymentTrigger newRemainingAmount newRemainingInstallments loan

 where
  payInstallment toPay =
    payToPublicKey_ defaultSlotRange (Ada.toValue toPay) (loaneePubKey loan)

calculateInstallmentValue :: Ada -> Integer -> InterestRate -> Ada
calculateInstallmentValue remainderToPay remainingInstallments interestRate
  | installmentWithInterest `isLargerThan` (adaToPreludeFloat remainderToPay)
  = remainderToPay
  | otherwise
  = Ada.lovelaceOf . ceiling $ installmentWithInterest
 where
  installmentAmount :: HP.Float
  installmentAmount =
    (adaToPreludeFloat remainderToPay) / fromIntegral remainingInstallments
  installmentWithInterest :: HP.Float
  installmentWithInterest = installmentAmount `addInterestToFloat` interestRate

-- We've "imported" some of the standard Prelude operators due to weird AdditiveSemigroup and MultiplicativeSemigroup errors
-- In essence, the pure number calculations are taken to the standard Haskell world
withInterestRounded :: Ada -> InterestRate -> Ada
withInterestRounded ada interestRate =
  (adaToPreludeFloat ada) `addInterestAndRound` interestRate

addInterestAndRound :: HP.Float -> InterestRate -> Ada
addInterestAndRound adaAsFloat interestRate =
  Ada.lovelaceOf . ceiling $ adaAsFloat `addInterestToFloat` interestRate

addInterestToFloat :: HP.Float -> InterestRate -> HP.Float
addInterestToFloat float interestRate = do
  let interestAmount = (float / 100) `preludeMult` (fromIntegral interestRate)
  float `preludeAdd` interestAmount

adaToPreludeFloat :: Ada -> HP.Float
adaToPreludeFloat = fromIntegral . toInteger

isLargerThan :: HP.Ord a => a -> a -> Bool
isLargerThan = (HP.>)

preludeAdd :: HP.Num a => a -> a -> a
preludeAdd = (HP.+)

preludeMult :: HP.Num a => a -> a -> a
preludeMult = (HP.*)

-- Debugging purposes
logValue :: (MonadWallet m, Show a) => Text -> a -> m ()
logValue valueName value = logMsg $ valueName `cT` ": " `cT` tshow value

tshow :: Show a => a -> Text
tshow = T.pack . show

cT :: Text -> Text -> Text
cT a b = T.pack $ T.unpack a ++ T.unpack b

$(mkFunctions
    ['lendLoan,
     'requestLoan
    ])
